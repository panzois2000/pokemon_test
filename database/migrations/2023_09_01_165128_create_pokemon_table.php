<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
{
    Schema::create('pokemon', function (Blueprint $table) {
        $table->id();
        $table->integer('pokemon_id');
        $table->string('name');
        $table->string('url');
        $table->string('weight');
        $table->string('height');
        $table->string('image')->nullable();
        $table->string('hp');
        $table->string('attack');
        $table->string('defense');
        $table->string('special_attack');
        $table->string('special_defense');
        $table->string('speed');
        $table->string('abilities');
        $table->string('types');
        // Add other columns for Pokemon details as needed
        $table->timestamps();
    });
}


    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pokemon');
    }
};
