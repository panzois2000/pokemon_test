# Web Application Documentation

## Table of Contents

1. **Introduction**

   - This is a webapp for retrieving and picking favorite pokemons. The app consist of a login/signup functionality. After logging in the user has the opportunity to see all pokemons available in pokeapi this current period (data update every week) and search for desired ones and store them as favorite.
   - The technologies used are:
     - **PHP** Laravel for the backend management along with **composer**
     - **React.js** and **Vite** along with the node package manager (**NPM**) for the frontend management and UI
     - **SQLite** for the database and the data managament in general

2. **Getting Started**

   1. **Server Setup:**

      - Create a new laravel application:

        ``````pseudocode
        composer global require laravel/installer
        laravel new <project-name>
        ``````

      - SQLite is used for simplicity:

        In the .env file change DB_connection from mysql to sqlite and comment out the rest DB variables

      - Create the database:

        ``````pseudocode
        php artisan migrate
        ``````

      - Run server

        ``````pseudocode
        php artisan serve
        ``````

      - Go to localhost:8000 (usually or else what APP_URL in .env says)

   2. **Frontend Setup**

      - Install node.js 

      - Install Vite:

        ```pseudocode
        npm create vite
        ```

        and choose react and javascript

      - Inside the frontend project run:

        ```pseudocode
        npm install
        npm run dev
        ```

      - Open the link appearing in terminal port 5173 or change it through package.json scripts.dev -> "vite --port=3000"

3. **Backend (PHP Laravel)**

   - **Structure:**
     
     - For this app there are three Controllers 
     
       - **AuthController** (signup, login, logout)
     
       - **PokemonController** (retrive all pokemons)
     
       - **FavoritesController** (add favorite, remove favorite, get all favorites)
     
         All are in api.php as routes
     
     - For the scheduled request responsible is
     
       - **/Console/Commands/PokeApi** which if triggered retrives all pokemons that are in pokeapi and with a system cronjob can be triggered weekly or at a specific time
     
     - There are also 2 models for the db
     
       - **Favorite**
       - **User** 
       - Pokemon was not essential to be stored as a model but it can be
     
   - **Authentication:**
     
     - The authentication process is using token and encrypting the password (insertion and token creation for signup, lookup for login, token deletion for logout)
     
   - **Database:**
     
     - There are 3 tables:
       - **Users** (holding information for every user)
       - **Pokemon** (holding information for every user)
       - **Favorites** (holding every favorite pokemon for every user using the email, and the pokemon_id)

   3. **Frontend (React.js)**

      - **Project Structure:**
        - Frontend has 2 general pages guest and default layout. 
          - If users are logged in and have access token then they are led to default layout where they see available pokemons in cards page or by click in "Favorites" button the favorite pokemons. Every pokemon card has some useful data and is presented as a card with like button. If users press this button the card is automatically stored in favorites table and appears in favorites page.
          - On the other case, they are led to login page in order put their credentials or click to "create account and sign up"
      - **Searching**
        - Pokemons page has a search bar where users can search in real time for spacific pokemons based on their name
      - **Data Fetching:**
        - Requests are made through Axios from backend and the updates are made asynchronously without reloading but rendering the responsible components.
