import {Navigate, createBrowserRouter} from "react-router-dom";
import Signup from "./pages/Signup";
import Login from "./pages/Login";
import NotFound from "./pages/NotFound";
import DefaultLayout from "./components/DefaultLayout";
import Cards from "./pages/Cards";
import GuestLayout from "./components/GuestLayout";
import Favorites from "./pages/Favorites";

const router = createBrowserRouter([
    {
        path: '/',
        element: <DefaultLayout/>,
        children: [
            {
                path: '/',
                element: <Navigate to="/cards" />
            },
            {
                path: '/cards',
                element: <Cards />
            },
            {
                path: '/favorites',
                element: <Favorites />
            }
        ]
    },
    {
        path: '/',
        element: <GuestLayout/>,
        children: [
            {
                path: '/login',
                element: <Login />
            },
            {
                path: '/signup',
                element: <Signup />
            }
        ]
    },
    {
        path: '*',
        element: <NotFound />
    }
])

export default router;
