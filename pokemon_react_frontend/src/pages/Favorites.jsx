import { useEffect, useState } from "react";
import axiosClient from "../axios_client";
import Card from "./Card";
import { useStateContext } from "../contexts/ContextProvider";

export default function Favorites() {
    const { setUser } = useStateContext();
    const [pokemons, setPokemons] = useState([]);
    const [offset] = useState(0);

    useEffect(() => {
        axiosClient.get('/user').then(({data}) => {
            setUser(data);
            console.log(data.email);
            const payload = {
                email: data.email,
            };
            axiosClient.get(`/find_favorite/?email=${payload.email}`).then(({data}) => {
                console.log(data);
                setPokemons(data);
            });
        });
    }, [offset]);

    return (
        <div style={{ display: "flex", flexDirection: "column", placeItems: "center" }}>
            {pokemons.length !== 0 ? (
                pokemons.map((pokemon, index) => (
                    <Card key={index}
                        url={pokemon[0].url}
                        id={pokemon[0].pokemon_id}
                        name={pokemon[0].name}
                        image={pokemon[0].image}
                        height={pokemon[0].height}
                        weight={pokemon[0].weight}
                        hp={pokemon[0].hp}
                        attack={pokemon[0].attack}
                        defense={pokemon[0].defense}
                        special_attack={pokemon[0].special_attack}
                        special_defense={pokemon[0].special_defense}
                        speed={pokemon[0].speed}
                        abilities={JSON.parse(pokemon[0].abilities)}
                        types={JSON.parse(pokemon[0].types)}
                        selected={true}
                    />
                ))
            ):(
                <h2>No favorite pokemons yet...</h2>
            )}
        </div>
    )
}
