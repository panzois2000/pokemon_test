import { useEffect, useState } from "react";
import axiosClient from "../axios_client";
import { Heart, HeartFill } from 'react-bootstrap-icons';
import { useStateContext } from "../contexts/ContextProvider";
import PropTypes from 'prop-types';

Card.propTypes = {
    selected: PropTypes.bool,
};
export default function Card(props) {
    const [pokemon, setPokemon] = useState({});
    const {user} = useStateContext();
    const [selected, setSelected] = useState(props.selected);
    const colors = ["red", "green", "blue", "yellow"]

    function add_remove_favorite(){
        if(selected){
            const payload = {
                email: user.email,
                id: pokemon.id
            }
            axiosClient.post('/remove_favorite', payload).then(()=>{
                setSelected(false);
                if(window.location.pathname=="/favorites"){
                    window.location.reload(false);
                }
            })
        }else{
            const payload = {
                email: user.email,
                id: pokemon.id,
                url: pokemon.url
            }
            axiosClient.post('/add_favorite', payload).then(({data})=>{
                console.log(data)
                setSelected(true);
            })
        }
    }

    useEffect(() => {
        // console.log(props.name + ": " + props.selected)
        setPokemon(props)
    },[props]);

    return (

        <div className="card mb-3" style={{maxWidth: "800px", width: "80%", backgroundColor: "lightgrey"}}>
        <div className="row g-0">
        <div className="col-md-5" style={{padding: "3% 6%", borderRadius: "10%", placeItems: "center", display: "flex", flexDirection: "column", justifyContent: "center", backgroundColor: "white", position: "relative"}}>
            <div style={{borderRadius: "100%", backgroundColor:"#EDEADE", width: "55px", height: "55px", textAlign: "center", paddingTop: "15px", position: "absolute", left: "-10%", top: "-10%"}}>{pokemon.height} m</div>
            <div style={{borderRadius: "100%", backgroundColor:"#EDEADE", width: "55px", height: "55px", textAlign: "center", paddingTop: "15px", position: "absolute", right: "-10%", top: "-10%"}}>{pokemon.weight} kg</div>
            <h1 style={{fontSize:"3rem", opacity: "0.15", position: "absolute", left: "50%", top: "50%", transform: "translate(-50%, -80%)"}}>#{pokemon.id}</h1>
            <img src={pokemon.image} className="img-fluid rounded-start" alt=""/>
            <h4 className="card-title" style={{textTransform: "capitalize", textAlign: "center"}}>{pokemon.name}</h4>
            <p className="card-title" style={{textTransform: "capitalize"}}>{pokemon.abilities?.map((ability, index) => (
                <span key={index} style={{textTransform: "capitalize", color: colors[index%4]}}> {ability} </span>
            ))}
            </p>
            <p className="card-title" style={{textTransform: "capitalize"}}>{pokemon.types?.map((type, index) => (
                <span key={index}> {type} </span>
            ))}
            </p>
        </div>
        <div className="col-md-6" style={{marginLeft: "5%", display: "flex", placeItems: "center"}}>
            <div className="card-body" style={{display: "flex", placeItems: "center"}}>
                <table className="table table-md" style={{backgroundColor: "lightgrey !important"}}>
                <thead>
                    <tr>
                    </tr>
                </thead>
                <tbody style={{borderW:"lightgray"}}>
                    <tr>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px", textAlign: "right", width:"20%"}}>Hp</td>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px", width:"5%"}}>{pokemon.hp}</td>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px"}}>
                        <div className="d-none d-sm-block d-md-none d-lg-block" style={{marginLeft: "5px", backgroundColor: "white", height: "14px", width: "100%"}}>
                            <div style={{backgroundColor: "grey", height: "14px", width: pokemon.hp*100/260.0+"%"}}></div>
                        </div>
                    </td>
                    </tr>
                    <tr>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px", textAlign: "right",}}>Attack</td>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px"}}>{pokemon.attack}</td>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px"}}>
                        <div className="d-none d-sm-block d-md-none d-lg-block" style={{marginLeft: "5px", backgroundColor: "white", height: "14px", width: "100%"}}>
                            <div style={{backgroundColor: "blue", height: "14px", width: pokemon.attack*100/260.0+"%"}}></div>
                        </div></td>
                    </tr>
                    <tr>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px", textAlign: "right",}}>Special Attack</td>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px"}}>{pokemon.special_attack}</td>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px"}}>
                        <div className="d-none d-sm-block d-md-none d-lg-block" style={{marginLeft: "5px", backgroundColor: "white", height: "14px", width: "100%"}}>
                            <div style={{backgroundColor: "red", height: "14px", width: pokemon.special_attack*100/260.0+"%"}}></div>
                        </div></td>
                    </tr>
                    <tr>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px", textAlign: "right",}}>Special Defense</td>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px"}}>{pokemon.special_defense}</td>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px"}}>
                        <div className="d-none d-sm-block d-md-none d-lg-block" style={{marginLeft: "5px", backgroundColor: "white", height: "14px", width: "100%"}}>
                            <div style={{backgroundColor: "orange", height: "14px", width: pokemon.special_defense*100/260.0+"%"}}></div>
                        </div></td>
                    </tr>
                    <tr>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px", textAlign: "right",}}>Speed</td>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px"}}>{pokemon.speed}</td>
                    <td style={{backgroundColor:"lightgray", borderWidth: "0px"}}>
                        <div className="d-none d-sm-block d-md-none d-lg-block" style={{marginLeft: "5px", backgroundColor: "white", height: "14px", width: "100%"}}>
                            <div style={{backgroundColor: "purple", height: "14px", width: pokemon.speed*100/260.0+"%"}}></div>
                        </div></td>
                    </tr>
                </tbody>
                </table>
            {/* <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p> */}
            </div>
        </div>
        <button onClick={add_remove_favorite} style={{position: "absolute", bottom: "-1rem", right: "-1rem", width: "fit-content", height: "fit-content", padding: "10px 15px", borderRadius:"100%", borderWidth: "0"}}>
            {selected? <HeartFill style={{color:"red", width: "30px", height: "30px" }}/>:<Heart style={{color:"red", width: "30px", height: "30px"}}/>}</button>
    </div>
</div>
    )
}
