import { useEffect, useState } from "react";
import axiosClient from "../axios_client";
import Card from "./Card";
import { Pagination } from 'react-bootstrap';
import PropTypes from 'prop-types';

Cards.propTypes = {
    search: PropTypes.node,
};
export default function Cards(props) {
    // const {setUser} = useStateContext();
    const [pokemons, setPokemons] = useState([]);
    const [selected, setSelected] = useState([]);
    const [selectedflag, setSelectedflag] = useState(false);
    const [offset, setOffset] = useState(0);
    const [pokemon_length, setPokemonLength] = useState(0);
    // const [search, setSearch] = useState(null);

    useEffect(() => {
        axiosClient.get('/user').then(({data}) => {
            // setUser(data);
            const payload = {
                email: data.email,
            };
            axiosClient.get(`/find_favorite/?email=${payload.email}`).then(({data}) => {
                setSelected(data);
                setSelectedflag(true);
            });
        });
        if(props.search){
            axiosClient.get(`/pokemons/?search=${props.search}&offset=${offset}&limit=20`).then(({data}) => {
                setPokemons(data.pokemons);
            });
        }else{
            axiosClient.get(`/pokemons/?offset=${offset}&limit=20`).then(({data}) => {
                setPokemons(data.pokemons);
                setPokemonLength(data.count);
            });
        }
        // setSearch(props.search)
    }, [offset, props.search]);

    const checkSelected = (id) => {
        for (let i = 0; i < selected.length; i++) {
            if (selected[i][0].pokemon_id === id){
                return true;
            }
        }
        return false;
    }

    const getPokemonNames = (page) => {
        setSelectedflag(false);
        if (page === -1) {
            setOffset(offset + 20);
        } else if (page === -2) {
            setOffset(offset - 20);
        } else {
            setOffset(page);
        }
    }

    return (
        <div style={{display: "flex", flexDirection: "column", placeItems:"center"}}>
            {selectedflag && pokemons.map((pokemon, index) => (
                <Card key={index}
                url={pokemon.url}
                id={pokemon.pokemon_id}
                name={pokemon.name}
                image={pokemon.image}
                height={pokemon.height}
                weight={pokemon.weight}
                hp={pokemon.hp}
                attack={pokemon.attack}
                defense={pokemon.defense}
                special_attack={pokemon.special_attack}
                special_defense={pokemon.special_defense}
                speed={pokemon.speed}
                abilities={JSON.parse(pokemon.abilities)}
                types={JSON.parse(pokemon.types)}
                selected={checkSelected(pokemon.pokemon_id)} />
            ))}


            <Pagination>
                <Pagination.First onClick={() => getPokemonNames(0)} />
                <Pagination.Prev onClick={() => getPokemonNames(-2)} />
                <Pagination.Next onClick={() => getPokemonNames(-1)} />
                <Pagination.Last onClick={() => getPokemonNames(pokemon_length-20)} />
            </Pagination>
        </div>
    )
}
