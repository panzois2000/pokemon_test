import { Navigate, Outlet } from "react-router-dom";
import { useStateContext } from "../contexts/ContextProvider";

export default function GuestLayout() {
    const {token} = useStateContext();
    // eslint-disable-next-line no-debugger
    if(token) {
        return <Navigate to="/"/>
    }

    return (
        <div>
            <Outlet/>
        </div>
    )
}
