import { Link, Navigate, Outlet } from "react-router-dom";
import { useStateContext } from "../contexts/ContextProvider";
import { useEffect, useState } from "react";
import axiosClient from "../axios_client";
import Cards from "../pages/Cards";
export default function DefaultLayout() {
    const { user, token, setUser, setToken } = useStateContext();
    const [search, setSearch] = useState(null);
    const [path, setPath] = useState(window.location.pathname);

    const onLogout = (ev) => {
        ev.preventDefault();
        axiosClient.post('/logout').then(() => {
            setUser({});
            setToken(null);
        });
    }

    const handleLinkClick = (newPath) => {
        setPath(newPath);
    }

    const searching = (event) => {
        const value = event.target.value;
        setSearch(value);
    }

    useEffect(() => {
        axiosClient.get('/user').then(({ data }) => {
            setUser(data);
        });
    }, [path]);

    if (!token) {
        return <Navigate to="/login" />;
    }

    return (
        <div id="defaultLayout">
            <aside>
                <Link onClick={() => handleLinkClick("/cards")} to="/cards">Cards</Link>
                <Link onClick={() => handleLinkClick("/favorites")} to="/favorites">Favorites</Link>
            </aside>
            <div className="content">
                <header>
                    <div>
                        My Pokemon store
                    </div>
                    <div className="form-inline d-flex justify-content-center md-form form-sm">
                        {path === "/cards" || path ==="/" ? (
                            <input
                                onChange={searching}
                                id="search"
                                className="form-control form-control-sm mr-3 w-75"
                                type="text"
                                placeholder="Search"
                                aria-label="Search"
                            />
                        ) : null}
                    </div>
                    <div>
                        {user.name}
                        <a href="#" onClick={onLogout} className="bttn-logout">Logout</a>
                    </div>
                </header>
                <main>
                    {path === "/cards" || path ==="/" ? (
                        <Cards search={search} />
                    ) : (
                        <Outlet />
                    )}
                </main>
            </div>
        </div>
    )
}
