<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    use HasFactory;

    protected $table = 'favorites'; // Specify the table name

    protected $fillable = ['email', 'pokemon_id', 'url']; // Define the fillable attributes
}
