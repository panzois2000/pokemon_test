<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PokemonController extends Controller
{
    public function index(Request $request)
    {
        $offset = $request->input('offset', 0); // Get the offset from the request (default to 0 if not provided)
        $search = $request->input('search');
        if($search){
            $pokemons = DB::table('pokemon')
                ->where('name', 'like', $search . '%')
                ->skip($offset)
                ->take(20) // Limit to 20 records
                ->get();
        }else{
            $pokemons = DB::table('pokemon')
                ->skip($offset)
                ->take(20) // Limit to 20 records
                ->get();
        }
        $count = DB::table('pokemon')->count();
        return response()->json(['count'=> $count, 'pokemons' => $pokemons]);
    }
}
