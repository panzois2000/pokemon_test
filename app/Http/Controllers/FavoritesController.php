<?php

namespace App\Http\Controllers;

use App\Models\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FavoritesController extends Controller
{
    public function add(Request $request)
    {
        $pokemon = DB::table('favorites')->where('email', $request->input('email'))->where('pokemon_id', $request->input('id'))->first();
        if ($pokemon) {
            return response()->json(['message' => 'Pokemon already exists']);
        }
        $favorite = DB::table('favorites')
        ->insert(
            [
                'email' => $request->input('email'),
                'pokemon_id' => $request->input('id'),
                'url'=> $request->input('url')
            ]
        );
        return response()->json(['message' => 'Pokemon added successfully'] );

    }

    public function remove(Request $request)
    {
        $pokemon = Favorite::where('pokemon_id', $request->input('id'))->first();
        if ($pokemon) {
            $pokemon->delete(); // Delete the found Pokemon
            return response()->json(['message' => 'Pokemon deleted successfully']);
        }
        return response()->json(['message' => 'Pokemon not found'], 404);
    }

    public function find(Request $request)
    {
        $pokemons = DB::table('favorites')->where('email', $request->input('email'))->get();
        $detailed_pokemons = [];
        foreach($pokemons as $pokemon){
            $tmp = DB::table('pokemon')->where('pokemon_id', $pokemon->pokemon_id)->get();
            array_push($detailed_pokemons, $tmp);
        }
        return response()->json($detailed_pokemons);
    }
}
