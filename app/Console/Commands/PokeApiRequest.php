<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PokeApiRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pokeapi:request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a request to PokeAPI';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        function get_pokemon_types($words){
            $types = [];
            $index=0;
            for ($index = 0; $index < count($words); $index++) {
                array_push($types, $words[$index]->type->name);
            }

            return $types;
        }

        function get_pokemon_abilities($words){
            $abilities = [];
            $index=0;
            for ($index = 0; $index < count($words); $index++) {
                array_push($abilities, $words[$index]->ability->name);
            }

            return $abilities;
        }

        $url = 'https://pokeapi.co/api/v2/pokemon/?limit=10000';
        $response = file_get_contents($url);
        $data = json_decode($response);

        if (!empty($data->results)) {
            DB::table('pokemon')->delete();
            foreach ($data->results as $pokemon) {

                $pokemon_response = file_get_contents($pokemon->url);
                $pokemon_data = json_decode($pokemon_response);
                // Save each Pokemon into the 'pokemon' table
                $pokemon->pokemon_id= $pokemon_data->id;
                $pokemon->weight= $pokemon_data->weight;
                $pokemon->height= $pokemon_data->height;
                $pokemon->image= $pokemon_data->sprites->front_default;
                $pokemon->hp= $pokemon_data->stats[0]->base_stat;
                $pokemon->attack= $pokemon_data->stats[1]->base_stat;
                $pokemon->defense= $pokemon_data->stats[2]->base_stat;
                $pokemon->special_attack= $pokemon_data->stats[3]->base_stat;
                $pokemon->special_defense= $pokemon_data->stats[4]->base_stat;
                $pokemon->speed= $pokemon_data->stats[5]->base_stat;
                $pokemon->abilities= json_encode(get_pokemon_abilities($pokemon_data->abilities));
                $pokemon->types= json_encode(get_pokemon_types($pokemon_data->types));
            }
            usort($data->results, function($a, $b) {return (float)$a->weight > (float)$b->weight;});
            foreach ($data->results as $pokemon) {
                DB::table('pokemon')->insert([
                    'pokemon_id' => $pokemon->pokemon_id,
                    'name' => $pokemon->name,
                    'url' => $pokemon->url,
                    'weight' => $pokemon->weight,
                    'height' => $pokemon->height,
                    'image' => $pokemon->image,
                    'hp' => $pokemon->hp,
                    'attack' => $pokemon->attack,
                    'defense' => $pokemon->defense,
                    'special_attack' => $pokemon->special_attack,
                    'special_defense' => $pokemon->special_defense,
                    'speed' => $pokemon->speed,
                    'abilities' => $pokemon->abilities,
                    'types' => $pokemon->types,
                ]);
            }
        }
    }
}
